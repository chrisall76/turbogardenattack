﻿using UnityEngine;
using System.Collections;

public class HowToPlay : MonoBehaviour {

	private Vector3 scale;
	public float originalWidth = 1280.0f;
	public float originalHeight = 720.0f;

	public MainMenuGUI menuGUI;

	public Rect backRect;
	public Rect areaOneRect;
	public Rect areaTwoRect;

	void Start () {
	
	}

	void Update () {
	
	}

	void OnGUI(){
		scale.x = Screen.width / originalWidth;
		scale.y = Screen.height / originalHeight;
		scale.z = 1.0f;
		var svMat = GUI.matrix;
		GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);

		if(GUI.Button(backRect, "Back")){
			menuGUI.enabled = true;
			this.enabled = false;
		}

		GUILayout.BeginArea (areaOneRect);

		GUILayout.Label ("HOW TO PLAY");

		GUILayout.Space (10);

		GUILayout.Label ("PLACING UNITS:");
		GUILayout.Label ("To place a Unit, click on a tile. Green for player 1(Host), Red for player 2 (client).");

		GUILayout.Space (10);
		
		GUILayout.Label ("UNIT ACTIONS:");
		GUILayout.Label ("Units have three (3) possible actions: destroy, defend, and attack");
		GUILayout.Label ("Destroy will destroy the unit and give back half the amounts needed to create it.");
		GUILayout.Label ("Defend will make the unit go into a defensive stance, cutting damage done to it in half. The tradeoff" +
			" is that you can't attack in this pose. Special blockers like the potato can cut damage times 4.");
		GUILayout.Label ("Attacking will attack a unit, and damage done is based on its tile row and its defense stat.");

		GUILayout.Space (10);

		GUILayout.EndArea ();
		GUILayout.BeginArea (areaTwoRect);

		GUILayout.Label ("DEEPER IN:");

		GUILayout.Label ("Each unit has stats, the main ones being attk (AKA attack) and def (AKA defense)");
		GUILayout.Label ("Attack won't be constant due to a enemy units defense stat, depending on the defense stat " +
			"attack can be halfed or even more.");
		GUILayout.Label ("When a unit goes into a defense stance, it gains SP. Def (AKA special defense). As long as there" +
			" in this pose they gain twice the regular amount of defense they had.");

		GUILayout.EndArea ();

		GUI.matrix = svMat; // restore matrix
	}
}
