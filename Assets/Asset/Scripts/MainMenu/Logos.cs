﻿using UnityEngine;
using System.Collections;

public class Logos : MonoBehaviour {

	private Vector3 scale;
	public float originalWidth = 1280.0f;
	public float originalHeight = 720.0f;

	public Texture2D OGAM;
	public Texture2D SCG;
	public Texture2D Donate;
	public Texture2D Logo;

	public Rect oGamRect;
	public Rect sCGRect;
	public Rect dCGRect;
	public Rect logoRect;

	void Start () {
	
	}

	void Update () {
	
	}

	void OnGUI(){
		scale.x = Screen.width / originalWidth;
		scale.y = Screen.height / originalHeight;
		scale.z = 1.0f;
		var svMat = GUI.matrix;
		GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);

		if (GUI.Button (oGamRect, OGAM)) {
			Application.ExternalEval("window.open('http://www.onegameamonth.com/','_blank')");
		}
		if (GUI.Button (sCGRect, SCG)) {
			Application.ExternalEval("window.open('http://www.charitygamejam.com/','_blank')");
		}
		if (GUI.Button (dCGRect, Donate)){
			Application.ExternalEval("window.open('http://www.gamesforchange.org/donate-now/','_blank')");
		}

		GUI.Label (logoRect, Logo);


		GUI.matrix = svMat; // restore matrix
	}
}
