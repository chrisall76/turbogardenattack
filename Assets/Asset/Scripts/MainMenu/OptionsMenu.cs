﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour {

	private Vector3 scale;
	public float originalWidth = 1280.0f;
	public float originalHeight = 720.0f;

	public MainMenuGUI menuGUI;

	public Rect backRect;
	public Rect areaOneRect;
	public Rect areaTwoRect;

	void Start () {
	
	}

	void Update () {
	
	}

	void OnGUI(){
		scale.x = Screen.width / originalWidth;
		scale.y = Screen.height / originalHeight;
		scale.z = 1.0f;
		var svMat = GUI.matrix;
		GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);

		if(GUI.Button(backRect, "Back")){
			menuGUI.enabled = true;
			this.enabled = false;
		}

		string[] names = QualitySettings.names;
		int i = 0;
		GUILayout.BeginArea (areaOneRect);

		GUILayout.Label ("Overall Quality " + QualitySettings.GetQualityLevel());
		PlayerPrefs.SetInt("QualLevel", QualitySettings.GetQualityLevel());
		while (i < names.Length) {
			if (GUILayout.Button(names[i]))
				QualitySettings.SetQualityLevel(i, true);
			
			i++;
		}

		GUILayout.EndArea();
		GUILayout.BeginArea (areaTwoRect);

		GUILayout.Label ("AA " + QualitySettings.antiAliasing);
		if (GUILayout.Button ("Increase")) {
			if(QualitySettings.antiAliasing == 4){
				QualitySettings.antiAliasing = 8;
			}
			if(QualitySettings.antiAliasing == 2){
				QualitySettings.antiAliasing = 4;
			}
			if(QualitySettings.antiAliasing == 0){
				QualitySettings.antiAliasing = 2;
			}
		}
		if (GUILayout.Button ("Decrease")) {
			if(QualitySettings.antiAliasing == 2){
				QualitySettings.antiAliasing = 0;
			}
			if(QualitySettings.antiAliasing == 4){
				QualitySettings.antiAliasing = 2;
			}
			if(QualitySettings.antiAliasing == 8){
				QualitySettings.antiAliasing = 4;
			}
		}
		PlayerPrefs.SetInt("AA", QualitySettings.antiAliasing);

		GUILayout.Label (QualitySettings.anisotropicFiltering + " AF");
		if (GUILayout.Button ("On")) {
			QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
		}
		if (GUILayout.Button ("Off")) {
			QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
		}

		GUILayout.Label ("Shadow Distance " + QualitySettings.shadowDistance.ToString("F0"));
		QualitySettings.shadowDistance = GUILayout.HorizontalSlider(QualitySettings.shadowDistance, 0.0F, 200.0f);
		PlayerPrefs.SetFloat("ShadDist", QualitySettings.shadowDistance);

		GUILayout.EndArea();


		GUI.matrix = svMat; // restore matrix
	}
}
