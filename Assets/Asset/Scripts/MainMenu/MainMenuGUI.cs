﻿using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {
	
    private Vector3 scale;
    public float originalWidth = 1280.0f;
    public float originalHeight = 720.0f;

	public OptionsMenu optionsMenu;
	public HowToPlay HTP;
	public Rect areaOneRect;

	public Rect credits;
	
	void Start () {
		//Quality
		if (Application.platform != RuntimePlatform.WindowsWebPlayer) {
			QualitySettings.SetQualityLevel (PlayerPrefs.GetInt("QualLevel"));
			QualitySettings.antiAliasing = PlayerPrefs.GetInt("AA");
			QualitySettings.shadowDistance = PlayerPrefs.GetFloat("ShadDist");
		}
	}
	
	void Update () {
	
	}
	
	void OnGUI(){
        scale.x = Screen.width / originalWidth;
        scale.y = Screen.height / originalHeight;
        scale.z = 1.0f;
        var svMat = GUI.matrix;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);

		GUILayout.BeginArea (areaOneRect);
		if(GUILayout.Button("Play Online")){
			Application.LoadLevel("ServerChoose");
		}

		if(GUILayout.Button("How To Play")){
			HTP.enabled = true;
			this.enabled = false;
		}

		if(GUILayout.Button("Options")){
			optionsMenu.enabled = true;
			this.enabled = false;
		}

		if(GUILayout.Button("Quit")){
			Application.Quit();
		}

		GUILayout.EndArea ();

		GUILayout.BeginArea (credits);
		
		GUILayout.Label ("Credit to " + "Roald Strauss & James Opie on IndieGameMusic" + " for the music");
		
		GUILayout.EndArea ();
		
		GUI.matrix = svMat; // restore matrix
	}
}
