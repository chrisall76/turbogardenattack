﻿using UnityEngine;
using System.Collections;

public class ScoreHandler : MonoBehaviour {

	public int POUnits = 0;
	public int PTUnits = 0;

	public int POPoints = 0;
	public int PTPoints = 0;

	public int POHealth = 100;
	public int PTHealth = 100;
	
	//GUI
    private Vector3 scale;
	public GUISkin guiSkin;
    public float originalWidth = 1280.0f;
    public float originalHeight = 720.0f;
	
	public Rect POScore;
	public Rect PTScore;
	public Rect POHealthRect;
	public Rect PTHealthRect;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(POHealth <= 0 || PTHealth <= 0){
			Network.CloseConnection(Network.connections[0], true);
		}
	}
	
	public void updatePointsPO(int Point){
		networkView.RPC("UpdatePO", RPCMode.All, Point);
	}
	
	public void updatePointsPT(int Point){
		networkView.RPC("UpdatePT", RPCMode.All, Point);
	}

	public void updatePointsPOH(int Point){
		networkView.RPC("UpdatePOH", RPCMode.All, Point);
	}
	
	public void updatePointsPTH(int Point){
		networkView.RPC("UpdatePTH", RPCMode.All, Point);
	}

	public void updateU(int Point, int num){
		networkView.RPC("UpdatePU", RPCMode.All, Point, num);
	}
	
	[RPC]
	void UpdatePO(int points){
		POPoints += points;
	}
	
	[RPC]
	void UpdatePT(int points){
		PTPoints += points;
	}

	[RPC]
	void UpdatePOH(int take){
		POHealth -= take;
	}
	
	[RPC]
	void UpdatePTH(int take){
		PTHealth -= take;
	}

	[RPC]
	void UpdatePU(int Num, int Play){
		if (Play == 0) {
			POUnits += Num;
		}
		if (Play == 1) {
			PTUnits += Num;
		}
	}
	
	
	void OnGUI(){
        scale.x = Screen.width / originalWidth;
        scale.y = Screen.height / originalHeight;
        scale.z = 1.0f;
        var svMat = GUI.matrix;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);
		GUI.skin = guiSkin;

		GUI.Label(POScore, "Player One Score: " + POPoints.ToString());
		GUI.Label(PTScore, "Player Two Score: " + PTPoints.ToString());

		GUI.Label(POHealthRect, "Player One Health: " + POHealth.ToString());
		GUI.Label(PTHealthRect, "Player Two Health: " + PTHealth.ToString());

		GUI.matrix = svMat; // restore matrix
	}
}
