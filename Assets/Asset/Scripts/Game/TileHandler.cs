﻿using UnityEngine;
using System.Collections;

public class TileHandler : MonoBehaviour {
	
	public TileType tileType = TileType.PlayerOne;
	public bool UnitHere = false;
	public int Row = 1;

	public LayerMask mask;
	
	void Start () {
	}
	
	void LateUpdate () {
		RaycastHit hit;

		if (Physics.Raycast (transform.position + new Vector3 (0, -2f, 0), Vector3.up, out hit, 20, mask)) {
			UnitHere = true;
		}else{
			UnitHere = false;
		}
	}
}
