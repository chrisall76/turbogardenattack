﻿using UnityEngine;
using System.Collections;

public class PlayerHandler : MonoBehaviour {
	
	private bool Selected = false;
	private GameObject selected;
	private bool SelectedUnit = false;
	public GameObject selectedUnit;
	public TileType playerType = TileType.PlayerOne;
	public ScoreHandler scoreHandle;
	public GameObject Tile;
	public GameObject[] grassUnits;
	public GameObject[] grassUnitsTwo;
	public int[] unitCost;

	//Main Player
	public int Health = 100;

	//GUI
    private Vector3 scale;
	public GUISkin guiSkin;
    public float originalWidth = 1280.0f;
    public float originalHeight = 720.0f;
	
	public Texture2D[] Icons;
	public Rect posButtons;
	public Rect energyRect;
	
	//GUI Checking
	private bool GUIOne = false;
	private bool GUITwo = false;

	//GUI-ButtonPos
	public Rect RowRect;
	public Rect DefRect;
	public Rect DestroyRect;
	public Rect DefendRect;
	public Rect AttackRect;
	public Rect AttackPRect;
	
	//Energy
	public int EnergyMax = 200;
	public int Energy = 0;
	public int EnergyAdd = 25;
	private float nextDamageGet;
	public int betweenEnergyGet = 10;
	
	//Attacking
	public bool Targeting = false;
	public GameObject ObjTargeting;
	public GameObject Target;

	//Sounds
	public AudioClip AttkUnit;
	public AudioClip placedUnit;
	public AudioClip removedUnit;

	//Other Temp Stuff
	private bool CheckP = false;
	private TileHandler hitCol;

	//Attk Other Player
	private float nextAttk;
	public int betweenAttk = 5;
	
	void Start () {
		if(Network.isClient){
			playerType = TileType.PlayerTwo;
			networkView.RPC("ChangeName", RPCMode.All, "P2");
		}

		if(Network.isServer){
			playerType = TileType.PlayerOne;
			networkView.RPC("ChangeName", RPCMode.All, "P1");
		}
		scoreHandle = GameObject.Find ("SpawnCam").GetComponent<ScoreHandler>();
	}
	
	void Update () {

		if (CheckP == false) {
			CheckPlayer();
		}

		if (Energy <= EnergyMax) {
			if (Time.time >= nextDamageGet) {
					nextDamageGet = Time.time + betweenEnergyGet;
					Energy += EnergyAdd;
			}
		}
		if(Energy > EnergyMax){
			Energy = EnergyMax;
		}
		
		if ( Input.GetMouseButtonDown(0)){
			RaycastHit hit;
			Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 100.0f)){
				if(hit.collider.gameObject.tag == "Tile"){
					hitCol = hit.collider.GetComponent<TileHandler>();
					if(hitCol.tileType == playerType && hitCol.UnitHere == false){
						selected = hit.collider.gameObject;
						Selected = !Selected;
					}
				}

				if(hit.collider.gameObject.tag == "Unit"){
					if(hit.collider.gameObject.GetComponent<UnitHandler>().tileType == playerType){
						SelectedUnit = !SelectedUnit;
						selectedUnit = hit.collider.gameObject;
					}
					if(hit.collider.gameObject.GetComponent<UnitHandler>().tileType != playerType && Targeting == true){
						selectedUnit.GetComponent<UnitHandler>().Defending = false;
						selectedUnit.GetComponent<UnitHandler>().AttkS();
						Target = hit.collider.gameObject;
						ObjTargeting.GetComponent<UnitHandler>().Attacking = true;
						selectedUnit.GetComponent<UnitHandler>().SetTarget(Target);
						Targeting = false;
					}
				}
			}
		}

		if ( Input.GetMouseButtonDown(1)){
			GUIOne = false;
			Selected = false;
			SelectedUnit = false;
			selected = null;
			selectedUnit = null;
		}
	}
	
	void OnGUI(){
        scale.x = Screen.width / originalWidth;
        scale.y = Screen.height / originalHeight;
        scale.z = 1.0f;
        var svMat = GUI.matrix;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);

		GUI.skin = guiSkin;
		GUI.Label (energyRect, "ENERGY: " + Energy.ToString());

		if(Selected == true && selected != null && GUITwo == false){
			for(int x = 0; x < Icons.Length; x++){
				if (GUI.Button (new Rect (x * posButtons.width, posButtons.y, posButtons.width, posButtons.height), Icons[x])) {
						
					if(playerType == TileType.PlayerOne && Energy >= unitCost[x]){
						selected.GetComponent<TileHandler>().UnitHere = true;
						audio.PlayOneShot(placedUnit);
						NetworkViewID viewID = Network.AllocateViewID();
						//networkView.RPC("Instantiate", RPCMode.All, x, selected.transform.position, selected.transform.rotation, viewID);
						Network.Instantiate(grassUnits[x], selected.transform.position, selected.transform.rotation, 0);
					    Energy -= unitCost[x];
						scoreHandle.updateU(1, 0);
					}
				    if(playerType == TileType.PlayerTwo && Energy >= unitCost[x]){
						selected.GetComponent<TileHandler>().UnitHere = true;
						audio.PlayOneShot(placedUnit);
						NetworkViewID viewID = Network.AllocateViewID();
						//networkView.RPC("InstantiateT", RPCMode.All, x, selected.transform.position, selected.transform.rotation, viewID);
						Network.Instantiate(grassUnitsTwo[x], selected.transform.position, selected.transform.rotation, 0);
						Energy -= unitCost[x];
						scoreHandle.updateU(1, 1);
					}
					Selected = false;
				}

				GUI.Label(new Rect (x * posButtons.width, posButtons.y + 50, posButtons.width, posButtons.height), "Energy Needed: " + unitCost[x].ToString());
			}
		}
		
		if(SelectedUnit == true && selectedUnit != null && GUIOne == false){
			GUITwo = true;
			UnitHandler tempHandle = selectedUnit.GetComponent<UnitHandler>();
			GUI.Label(RowRect, "Current Row: " + tempHandle.Row);
			GUI.Label(DefRect, "Defense Stat: " + tempHandle.Defense);

			if(GUI.Button(DestroyRect, "Destroy")){
				if(playerType == TileType.PlayerOne){
					scoreHandle.updateU(-1, 0);
				}
				if(playerType == TileType.PlayerTwo){
					scoreHandle.updateU(-1, 1);
				}
				audio.PlayOneShot(removedUnit);
				tempHandle.tileHandler.UnitHere = false;
				Energy += (unitCost[tempHandle.Num]) / 2;

				if(networkView.isMine){
					Network.RemoveRPCs(selectedUnit.networkView.viewID);
					Network.Destroy(selectedUnit.networkView.viewID);
				}

				SelectedUnit = false;
				selectedUnit = null;
			}
			if(GUI.Button(DefendRect, "Defense Stance")){
				tempHandle.DefendS();
			}
			if(tempHandle.canAttk == true){
				if(GUI.Button(AttackRect, "Attack")){
					ObjTargeting = selectedUnit;
					Targeting = true;
				}
			}

			if(tempHandle.canAttk == true){
				if(GUI.Button(AttackPRect, "Attack Player")){
					if (Time.time >= nextAttk) {
						nextAttk = Time.time + betweenAttk;

						if(Network.isServer){
						     networkView.RPC("HurtOtherPlayer", RPCMode.All, 0);
						}
						if(Network.isClient){
							networkView.RPC("HurtOtherPlayer", RPCMode.All, 1);
						}
					}
				}
			}

		}else{
			GUITwo = false;
		}
		
		GUI.matrix = svMat; // restore matrix
	}

	void CheckPlayer(){
		if(networkView.isMine){
			Debug.Log("Mine!");
			gameObject.SetActive(true);
		}else{
			Debug.Log("Not mine!");
			if(playerType == TileType.PlayerOne){
			    playerType = TileType.PlayerTwo;
			}
			if(playerType == TileType.PlayerTwo){
				playerType = TileType.PlayerOne;
			}
			gameObject.SetActive(false);
		}
	}

	[RPC]
	void HurtOtherPlayer(int i){
		ScoreHandler SHandle = GameObject.Find ("SpawnCam").GetComponent<ScoreHandler>();
		if (i == 0) {
			if(scoreHandle.PTUnits == 0){
				audio.PlayOneShot(AttkUnit);
			    SHandle.updatePointsPTH(5);
			}
		}
		if (i == 1) {
			if(scoreHandle.POUnits == 0){
				audio.PlayOneShot(AttkUnit);
			    SHandle.updatePointsPOH(5);
			}
		}
	}

	[RPC]
	void Instantiate(int Num, Vector3 pos, Quaternion quad, NetworkViewID viewID){
		Debug.Log ("InstantiatedN!");
		GameObject temp;
		temp = GameObject.Instantiate(grassUnits[Num], pos, quad) as GameObject;
		temp.GetComponent<NetworkView>().viewID = viewID;
		temp.GetComponent<UnitHandler> ().Num = Num;
	}

	[RPC]
	void InstantiateT(int Num, Vector3 pos, Quaternion quad, NetworkViewID viewID){
		Debug.Log ("InstantiatedTN!");
		GameObject temp;
		temp = GameObject.Instantiate(grassUnitsTwo[Num], pos, quad) as GameObject;
		temp.GetComponent<NetworkView>().viewID = viewID;
		temp.GetComponent<UnitHandler> ().Num = Num;
	}


	[RPC]
	void ChangeName(string Name){
		gameObject.name = Name;
	}
}
