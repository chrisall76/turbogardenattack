﻿using UnityEngine;
using System.Collections;

public class UnitHandler : MonoBehaviour {
	
	private GameObject tempTarget;
	public GameObject Cam;
	public TileType tileType = TileType.PlayerOne;
	public UnitType unitType = UnitType.Grass;
	public GameObject levelTwoUnit;
	public int Level = 1;
	
	//Highlight
	public TileHandler tileHandler;
	public GameObject highlightObject;
	public Color mainColor;
	
	//Attacking/healing
	private int baseDef;
	public int Defense = 1;
	public int Health = 50;
	public int damage = 25;
	public bool Defending = false;
	public bool canAttk = true;
	public bool Attacking = false;
	public float inBetweenAttack = 1.5f;
	private float nextDamage;
	
	//Animation
	public Animator Anim;
	
	//Other
	public int Row;
	public GameObject damgCount;
	public GameObject target;
	public LayerMask mask;
	public int Num = 0;
	public ScoreHandler scoreHandle;
	
	void Start () {

		mainColor = highlightObject.renderer.material.color;
		scoreHandle = GameObject.Find ("SpawnCam").GetComponent<ScoreHandler> ();
		baseDef = Defense;

		if (unitType == UnitType.Sun) {
			InvokeRepeating("EnergyAdd", inBetweenAttack, inBetweenAttack);
		}
		
		RaycastHit hit;
		if(Physics.Raycast(transform.position + new Vector3(0, 2f, 0), -Vector3.up, out hit, 2.5f, mask)){
			if(hit.collider.tag == "Tile"){
				tileHandler = hit.collider.gameObject.GetComponent<TileHandler>();
				Row = hit.collider.gameObject.GetComponent<TileHandler>().Row;
				Debug.Log(hit.collider.gameObject.GetComponent<TileHandler>().Row.ToString());
			}
		}

		if (tileType == TileType.PlayerOne) {
			Cam = GameObject.Find ("P1");
		}
		
		if (tileType == TileType.PlayerTwo) {
			Cam = GameObject.Find("P2");
		}

	}
	
	void FixedUpdate () {

		if (Defending == true) {
			if(unitType == UnitType.DBlocker){
				Defense = baseDef * 3;
			}else{
			    Defense = baseDef * 2;
			}
		} else {
			Defense = baseDef;
		}

		if (Anim) {
			if (Defending == true) {
				Anim.SetBool ("Def", true);
			} else {
				Anim.SetBool ("Def", false);
			}
		}
		
		if(Health <= 0){
			tileHandler.UnitHere = false;
			if(tileType == TileType.PlayerOne){
				scoreHandle.updatePointsPT(10);
			}
			if(tileType == TileType.PlayerTwo){
				scoreHandle.updatePointsPO(10);
			}
			Network.RemoveRPCs(networkView.viewID);
			Network.Destroy(networkView.viewID);
		}
		
		if(Attacking == true && canAttk == true && target != null){
			if(Anim){
			    networkView.RPC("Attack", RPCMode.All);
			}
		}else{
			Attacking = false;
			networkView.RPC("SwitchToIdle", RPCMode.All);
		}
	}
	
	public void SetTarget(GameObject tar){
		tempTarget = tar;
		networkView.RPC("setTarg", RPCMode.All, tempTarget.networkView.viewID);
	}

	void EnergyAdd(){
		if (Defending == false) {
			Cam.GetComponent<PlayerHandler> ().Energy += damage;
		} else {
			Cam.GetComponent<PlayerHandler> ().Energy += damage / 2;
		}
	}

    void OnMouseOver() {
		highlightObject.renderer.material.color = mainColor + new Color(1.5f, 1.5f, 1.5f);
    }
	
	void OnMouseExit() {
		highlightObject.renderer.material.color = mainColor;
	}


	public void AttkS(){
		networkView.RPC ("Attk", RPCMode.All);
	}

	public void DefendS(){
		networkView.RPC ("Defend", RPCMode.All);
	}

	[RPC]
	void SwitchToIdle(){
		Anim.SetBool("Attk", false);
	}

	[RPC]
	void Defend(){
		Defending = !Defending;
	}

	[RPC]
	void Attk(){
		Attacking = true;
		Anim.SetBool ("Attk", true);
	}

	[RPC]
	void Attack(){
		if (Time.time >= nextDamage && target) {
			Attacking = true;
			int crit = 1;
			int critChance = 1;
			critChance = Random.Range(0, 10);

			if (target){
				if(critChance == 7){
					crit = 2;
				}else{
					crit = 1;
				}

				UnitHandler targUH = target.GetComponent<UnitHandler>();
				if (targUH.Defending == false) {
					if(targUH.tileHandler.Row == 1){
						GameObject DC = Instantiate(damgCount, target.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
						DC.rigidbody.AddForce(new Vector3(0, 10, 0));
						DC.GetComponent<TextMesh>().text = (damage * crit / targUH.Defense).ToString();
					    targUH.Health -= damage * crit / targUH.Defense;
					}else{
						GameObject DC = Instantiate(damgCount, target.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
						DC.rigidbody.AddForce(new Vector3(0, 10, 0));
						DC.GetComponent<TextMesh>().text = (damage * crit / (targUH.Defense * 2)).ToString();
						targUH.Health -= damage * crit / (targUH.Defense * 2);
					}
				} else {
					if(targUH.tileHandler.Row == 1){
						GameObject DC = Instantiate(damgCount, target.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
						DC.rigidbody.AddForce(new Vector3(0, 10, 0));
						DC.GetComponent<TextMesh>().text = (damage * crit / (targUH.Defense * 2)).ToString();
						targUH.Health -= damage * crit * crit / (targUH.Defense * 3);
					}
				}
			}else{
				Attacking = false;
			}

			nextDamage = Time.time + inBetweenAttack;
		}
	}
	
	[RPC]
	void setTarg(NetworkViewID ID){
		target = NetworkView.Find(ID).gameObject;
	}
}
